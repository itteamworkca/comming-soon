;(function() {
    'use strict';

    var currentDate = new Date();
    var futureDate  = new Date(currentDate.getFullYear(), 4, 7);

    var diff = futureDate.getTime() / 1000 - currentDate.getTime() / 1000;

    $('.dw_clock').FlipClock(diff, {
        clockFace: 'DailyCounter',
        countdown: true,
        showSeconds: true
    });
}());

!function(exports) {
  exports.submit = submit;

  function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  function submit(form, event) {
    event.preventDefault();
    var email = form.email.value.trim();

    if (!validateEmail(email)) {
        return false;
    }

    var firebase = new Firebase('https://itteamwork.firebaseio.com/');
    firebase.push({email: email, created_at: new Date().getTime()});

    firebase.on('child_added', function(snapshot) {
        exports.swal({
          title: 'Great!',
          text: 'Now we have your email and we will contact you as soon as possible! Thank you.',
          type: 'success',
          timer: 4500,
          showConfirmButton: false
        });
    });
  }
}(typeof module === 'undefined' ? window : module.exports);
